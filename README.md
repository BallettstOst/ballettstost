**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

<h1>Ballettstudio Ost | New Website</h1>


 		<a href="social-glossar.php" title="Informationen zu Ballett in Frankfurt">Social & Glossar</a> - 
		<a href="sitemap.html" title="Seitenverzeichnis">Site Map</a> -
		<a href="https://www.ballettstudio-ost.de/kurse.php#Preballett" title="Pre-Ballett Kurse in Frankfurt">Pre-Ballett</a> -
		<a href="https://www.ballettstudio-ost.de/kurse.php#Ballett-Kinder-6-8" title="Ballettkurse für Kinder in Frankfurt">Kinderballett</a> -
		<a href="https://www.ballettstudio-ost.de/kurse.php#Ballett-Jugendliche" title="Ballett für Jugendliche in Frankfurt">Jugendliche</a> -
		<a href="https://www.ballettstudio-ost.de/kurse.php#Ballett-Erwachsene" title="Ballett für Erwachsene in Frankfurt">Erwachsene</a> -
		<a href="https://www.ballettstudio-ost.de/online-kurse.php" title="Ballett Online Kurse in Frankfurt">Online Kurse</a> -
		<a href="https://www.ballettstudio-ost.de/faq.php" title="FAQ' s | Fragen zu Ballettthemen">FAQ' s</a>


<p>The <a href="https://www.ballettstudio-ost.de">Ballet Studio Ost</a> is located directly on Osthafenplatz in Frankfurt am Main. The range of courses includes ballet courses for almost all ages. 
The little ones can start pre-ballet from around three and a half years of age. 
They learn the first basic elements and simple step combinations of classical ballet in a playful, imaginative way.
The owner of the studio, Ms. Olga Leibrandt, has been offering ballet courses in the Frankfurt area for 12 years and is a qualified ballet dancer (trained at the national Bolshoi Ballet Academy in Moscow) 
and ballet teacher. A free trial lesson (up to the age of 16 inclusive) is possible at any time by appointment.</p>


1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).