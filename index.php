<!DOCTYPE html>
<html lang="de">
    <head>
    <link rel="canonical" href="https://www.ballettstudio-ost.de">
 	<meta http-equiv="content-type" content="text/html; charset=UTF-8">    
	<!########################### OLD Version live on https://www.ballettstudio-ost ######################-->

	 
    <!-- JSON-LD-Markup generiert von Google Strukturierte Daten: Markup-Hilfe -->
	<script type="application/ld+json">
{
  	"@context" : "http://schema.org",
  	"@type" : "LocalBusiness",
  	"name" : "Ballettstudio Ost Ballettschule fÃ¼r Kinder und Erwachsene",
  	"logo": "https://www.ballettstudio-ost.de/img/logo.png",
    "image" : "https://www.ballettstudio-ost.de/img/ballett-frankfurt.jpg",
  	"priceRange" : "$39 - $77",
    "description": "Ballett fÃ¼r Kinder und Erwachsene, Ballettkurse Frankfurt Ostend, Bornheim, Fechenheim, Nordend, Pre-Ballett",
  	"telephone" : "069-29721499",
  	"email" : "mailto:info@ballettstudio-ost.de",
    "sameAs" : [
    "https://twitter.com/BallettstOst",
    "https://www.facebook.com/Leibrandt.Olga",
    "https://www.Instagram.com/ballettstudio_ost",
    "https://www.linkedin.com/in/olga-leibrandt",
    "https://www.pinterest.de/BallettstOst",
    "https://www.youtube.com/channel/UCqJWmS0Z_F9rvZQp4IQATlg"
 	],
  	"address" : {
    "@type" : "PostalAddress",
    "streetAddress" : "Hanauer LandstraÃŸe 138",
    "addressLocality" : "Frankfurt am Main",
    "postalCode" : "60314"
 	 },
  	"url" : "https://www.ballettstudio-ost.de/"
	}      
   
    </script>
 
         
    	<meta name="robots" content="INDEX,FOLLOW">
    	<meta name="last-updated" content="2020-08-18 11:38:07 UTC">
    	<meta name="geo.region" content="DE-HE" />
    	<meta name="geo.placename" content="Frankfurt am Main" />
      	<meta name="geo.position" content="50.1115912;8.7109423" />
    	<meta name="ICBM" content="50.111634, 8.71317" />
    	<meta name="description" content="Ballettschule direkt am Osthafenplatz in Frankfurt â­ Ballettkurse fÃ¼r Kinder & Erwachsene â­ NEU: Ballett-Erwachsene (AnfÃ¤nger) mittwochs 19:30.">    	
		<meta name="author" content="Creactive 5 - www.creactive5.de">
		<meta name="thumbnail" content="https://www.ballettstudio-ost.de/img/ballettkurse.jpg" />
    	<meta name="publisher" content="https://www.ballettstudio-ost.de Olga Leibrandt"> 
        <meta name="copyright" content="https://www.ballettstudio-ost.de Olga Leibrandt">
      	<meta name="audience" content="Alle">
       	<meta name="page-topic" content="Sport">
   		<meta name="creation_date" content="2020-02-18">
       	<meta name="revisit-after" content="2 days">
       	<meta name="viewport" content="width=device-width, initial-scale=1">
   		<meta name="yandex-verification" content="79fd0fd58a99a030" />    
 		<meta property="og:title" content="Ballettschule Frankfurt ðŸ’ƒ Ballett fÃ¼r Kinder und Erwachsene"/>
		<meta property="og:description" content="Ballett Frankfurt, Kinderballett Frankfurt, Ballettkurse, Ballett fÃ¼r Kinder und Erwachsene" />
		<meta property="og:image" content="https://www.ballettstudio-ost.de/img/Screenshot-ballettstudio-ost.de.jpg"/>
		<meta property="og:type" content="website"/>
		<meta property="og:url" content="https://www.ballettstudio-ost.de/"/>
		<meta property="fb:app_id" content="112837343499653" /> 	
    	<meta property="og:image:alt" content="Ballettstudio Ost" />
		<meta property="og:site_name" content="Ballettstudio Ost" />
    	<meta name="twitter:card" content="summary">
		<meta name="twitter:site" content="@BallettstOst">
		<meta name="twitter:title" content="Ballettschule Frankfurt âšœï¸ Ballett fÃ¼r Kinder und Erwachsene">
		<meta name="twitter:description" content="Neu erÃ¶ffnete Ballettschule. Balletttkurse fÃ¼r Kinder- und Erwachsene, Ballett Frankfurt.">
    	<meta name="twitter:image" content="https://www.ballettstudio-ost.de/img/ort/vorzimmer.jpg">    

        <title>Ballettschule Frankfurt &#x1F483;| Ballett fÃ¼r Kinder & Erwachsene</title>
      
 	 	<link rel="preload" href="/shariff/fa-brands-400.woff2" as="font" type="font/woff2" crossorigin="anonymous">
 		<link rel="preload" href="/shariff/fa-solid-900.woff2" as="font" type="font/woff2" crossorigin="anonymous">
 		<link rel="preload" href="/shariff/fa-regular-400.woff2" as="font" type="font/woff2" crossorigin="anonymous">	
 		<link rel="preload" href="/public/schrift/SnellRoundhand.ttf" as="font" type="font/woff2" crossorigin="anonymous">       
        <link rel="stylesheet" href="public/css/bootstrap.min.css">
        <link rel="stylesheet" href="public/css/scrolling-nav.css">
        <link rel="stylesheet" href="public/css/blueimp-gallery.min.css">
        <link rel="stylesheet" href="public/css/ballett.min.css">
	
	<link rel="apple-touch-icon" sizes="57x57" href="/icons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/icons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/icons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/icons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/icons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/icons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/icons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/icons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/icons/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/icons/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/icons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/icons/favicon-16x16.png">
	<link rel="manifest" href="/icons/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/icons/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
		<body>
	
		<!-- ### Global site tag (gtag.js) - Google Analytics ### -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-151234545-1">
		</script>
		<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  		gtag('js', new Date());
  		gtag('config', 'UA-151234545-1');
		</script>
		<!--  ### Ende Google Analytics ### -->
		

        <?php 
            include("src/header.php");
            include("src/welcome.php");
            include("src/about.php");
            include("src/stundenplan.php");
            include("src/kurs.php");
            include("src/preis.php");
            include("src/shows.php");
            include("src/galery.php");
            include("src/contact.php");
            include("src/footer.php");
            include("src/scripts.php");
	   ?>
	</body>

</html>
